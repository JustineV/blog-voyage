<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context         = Timber::context();
$timber_post     = Timber::get_post();
$context['post'] = $timber_post;
$context['color_svg'] = $context['post']->get_field('color_svg');
$context['color_icon'] = $context['post']->get_field('color_icon');
$context['photo_header'] = $context['post']->get_field('photo_header');

/* Breadcrumb */
if ( function_exists('yoast_breadcrumb') ) {
  $context['breadcrumb'] = yoast_breadcrumb( '<p id="breadcrumbs">','</p>', false );
}

/* Category */
$categories = $timber_post->categories();
$context['category_slug'] = $categories[sizeof($categories)-1]->slug;

/* Intro */
$context['intro'] = $context['post']->get_field('intro');

/* Image transition */
$context['image_transition'] = $context['post']->get_field('image_transition');

/* Descrition */
$context['description'] = $context['post']->get_field('description');

/* Maps */
$context['maps']['shortcode'] = do_shortcode('[jv_blog_post_maps]');

/* Title interest's points */
$context['title_interests_points'] = $context['post']->get_field('title_interests_points');

/* Etapes */
$context['etapes']['subtitle'] = __('Les', 'jy-vais');
$context['etapes']['title'] = __('Étapes de la balade', 'jy-vais');
$context['etapes']['list'] = $context['post']->get_field('etapes');
foreach($context['etapes']['list'] as $key_etape => $etape){
  if($etape['title'] != "" && $etape['texte'] != ""){
    $context['etapes']['list'][$key_etape]['carousel_images'] = array();
    foreach($etape['images'] as $image){
      if( $image != "" || $image != false ){
        $context['etapes']['list'][$key_etape]['carousel_images'][] = wp_get_attachment_image($image, 'full');
      }
    }
  }
  else {
    unset($context['etapes']['list'][$key_etape]);
  }
}

/* Bottom desription */
$context['bottom_description'] = $context['post']->get_field('bottom_description');

/* Other posts */
$context['other_posts']['prev'] = get_previous_post();
if(is_object($context['other_posts']['prev'])){
  $context['other_posts']['prev_link'] = get_permalink($context['other_posts']['prev']->ID);
  $context['other_posts']['prev_image'] = get_the_post_thumbnail_url($context['other_posts']['prev']->ID);
}
$context['other_posts']['next'] = get_next_post();
if(is_object($context['other_posts']['next'])){
  $context['other_posts']['next_link'] = get_permalink($context['other_posts']['next']->ID);
  $context['other_posts']['next_image'] = get_the_post_thumbnail_url($context['other_posts']['next']->ID);
}


if ( post_password_required( $timber_post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $timber_post->ID . '.twig', 'single-' . $timber_post->post_type . '.twig', 'single-' . $timber_post->slug . '.twig', 'single.twig' ), $context );
}

function curl_get_contents($url)
{
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}
