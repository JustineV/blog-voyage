<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'archive.twig', 'index.twig' );

$context = Timber::context();

$context['title'] = 'Archive';
if ( is_day() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
} elseif ( is_month() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );
} elseif ( is_year() ) {
	$context['title'] = 'Archive: ' . get_the_date( 'Y' );
} elseif ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
} elseif ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	//array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
	array_unshift( $templates, 'category.twig' );
	$timber_post = new Timber\Term( get_queried_object_id() );
	$context['term'] = $timber_post;
	$category = get_queried_object();
	$base_dir_upload = wp_upload_dir();
	$cities = get_terms(array(
		'taxonomy' => 'villes',
		'fields' => 'ids'
	));
	$context['cities'] = array();
	
	foreach($cities as $key_city => $city){
		$categories_city = get_field('categorie', 'villes_'.$city);
		if($categories_city != null){
			foreach($categories_city as $category_city){
				if($category_city->term_id == get_queried_object_id()){
					$term_city = get_term($city);
					$context['cities'][$city]['name'] = $term_city->name;
					$context['cities'][$city]['slug'] = $term_city->slug;
					$context['cities'][$city]['left'] = get_field('left', "villes_".$city);
					$context['cities'][$city]['top'] = get_field('top', "villes_".$city);
					$context['cities'][$city]['city_posts'] = array();
				}
			}
		}		
	}
	if(!$category->parent){
		$context['map'] = $base_dir_upload['baseurl'].'/carte-'.$category->slug.'.png';
		$context['all_posts'] = new Timber\PostQuery(array('cat'=> $category->term_id, 'posts_per_page' => -1));
		foreach($context['all_posts'] as $index_post => $post){
			$categories = get_the_category($post->ID);
			$context['main_city'][$categories[0]->term_taxonomy_id][0]['name'] = $categories[0]->name;
			$context['main_city'][$categories[0]->term_taxonomy_id][0]['slug'] = strtolower(trim(preg_replace('/[\s-]+/', '-', preg_replace('/[^A-Za-z0-9-]+/', '-', preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $categories[0]->name))))), '-'));
			$context['main_city'][$categories[0]->term_taxonomy_id][0]['left'] = get_field('left', $categories[0]->taxonomy.'_'.$categories[0]->term_taxonomy_id);
			$context['main_city'][$categories[0]->term_taxonomy_id][0]['top'] = get_field('top', $categories[0]->taxonomy.'_'.$categories[0]->term_taxonomy_id);
			$context['main_city'][$categories[0]->term_taxonomy_id][$post->ID]['name'] = $post->name;
			$context['main_city'][$categories[0]->term_taxonomy_id][$post->ID]['permalink'] = get_permalink($post->ID);
			$terms_city = get_the_terms($post->ID, 'villes');
			foreach($terms_city as $term_city){
				$context['cities'][$term_city->term_id]['city_posts'][$post->ID]['name'] = $post->name;
				$context['cities'][$term_city->term_id]['city_posts'][$post->ID]['permalink'] = get_permalink($post->ID);
			}
		}
		if(isset($context['main_city'])){
			usort($context['main_city'], function($item1, $item2){
				return strcmp($item1[0]['name'], $item2[0]['name']);
			});
		}		
	}
	//var_dump($context['main_city']);
} elseif ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

/* Breadcrumb */
if ( function_exists('yoast_breadcrumb') ) {
  $context['breadcrumb'] = yoast_breadcrumb( '<p id="breadcrumbs">','</p>', false );
}
$context['posts'] = new Timber\PostQuery();
foreach($context['posts'] as $key => $post){
	if($key % 2 == 0){
		$context['posts_even'][] = $post;
	}
	else {
		$context['posts_odd'][] = $post;
	}
}

Timber::render( $templates, $context );
