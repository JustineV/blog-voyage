<?php
  //Register widgets
  function jyvais_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer Col 1 ',
        'id'            => 'footer_widgets_col_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="footer-title">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Col 2 ',
        'id'            => 'footer_widgets_col_2',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="footer-title">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Col 3 ',
        'id'            => 'footer_widgets_col_3',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="footer-title">',
        'after_title'   => '</p>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Col 4 ',
        'id'            => 'footer_widgets_col_4',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="footer-title">',
        'after_title'   => '</p>',
    ) );

}
add_action( 'widgets_init', 'jyvais_widgets_init' );
 ?>
