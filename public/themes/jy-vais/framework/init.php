<?php
  require_once('helpers/widgets.php');

  function jyvais_styles_and_scripts(){
    wp_enqueue_style('jy-vais',get_template_directory_uri().'/assets/styles/app.css');

    wp_enqueue_script('gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js');
    wp_enqueue_script('scroll-trigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/ScrollTrigger.min.js');
    wp_enqueue_script('jy-vais',get_template_directory_uri().'/assets/scripts/app.js', array('jquery'));
  }
  add_action('wp_enqueue_scripts','jyvais_styles_and_scripts');



      function add_style_attributes( $html, $handle ) {

          if ( 'leaflet' === $handle ) {
              return str_replace( "media='all'", "media='all' integrity='sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==' crossorigin=''", $html );
          }

          return $html;
      }
      add_filter( 'style_loader_tag', 'add_style_attributes', 10, 2 );

      function add_script_attributes( $html, $handle ) {

          if ( 'leaflet' === $handle ) {
              return str_replace( 'id="leaflet-js"', 'id="leaflet-js" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""', $html );
          }

          return $html;
      }
      add_filter( 'script_loader_tag', 'add_script_attributes', 10, 2 );

    // Custom front of search form
    function custom_search_form( $form ) {
        $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
            <div class="search-custom-form d-flex">
                <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'. esc_attr__( 'Search' ) .'" />
                <button type="submit" id="searchsubmit"><i class="material-icons search">search</i></button>
            </div>
        </form>';
  
        return $form;
    }
    add_filter( 'get_search_form', 'custom_search_form', 40 );

