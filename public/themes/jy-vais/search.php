<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'archive.twig', 'index.twig' );

$context          = Timber::context();
$context['title'] = 'Résultats de recherche pour : ' . get_search_query();
$context['posts'] = new Timber\PostQuery();

// Add to posts, the listing of category which correspond to the search query
$args = [
    'taxonomy'   => 'category',
    'search'     => get_search_query(),
];
$terms = get_terms( $args );
foreach($terms as $id_term => $term){
    $term->link = get_term_link($term->term_id);
    $term->title = $term->name;
    $term->excerpt = "Retrouvez tous les articles de la catégorie ".$term->name;
    $image = get_field('image', 'category_'.$term->term_id);
    $term->thumbnail = array(
        'src' =>$image['url'],
        'alt' =>$image['alt'],
    );
    $context['posts'][] = $term;
}

Timber::render( $templates, $context );
