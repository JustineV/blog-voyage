<?php
/**
* Plugin Name: JV Maps
* Description: Show maps on blog post
* Author:      J'y vais
*/

add_action(
  'plugins_loaded',
  array ( JV_Maps::get_instance(), 'plugin_setup' )
);

class JV_Maps
{
  private $cpt = 'post'; # Adjust the CPT
  protected static $instance = NULL;
  public $plugin_url = '';
  public function __construct() {}

    public static function get_instance()
    {
      NULL === self::$instance and self::$instance = new self;
      return self::$instance;
    }

    /**
    * Regular plugin work
    */
    public function plugin_setup()
    {
      $this->plugin_url = plugin_dir_url( __FILE__ );
      add_shortcode('jv_blog_post_maps', array($this, 'shortcode_blog_post_maps'));
      add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
      add_action( 'wp_ajax_query_rand_post', array( $this, 'query_rand_post' ) );
      add_action( 'wp_ajax_nopriv_query_rand_post', array( $this, 'query_rand_post' ) );
    }

    /**
    * ACTION Enqueue scripts
    */
    public function enqueue()
    {
      wp_enqueue_style( 'leaflet-css', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css' );;
      wp_enqueue_script( 'leaflet-js', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', array('jquery'), '1.0', true);
      wp_enqueue_script( 'jv-ajax', plugin_dir_url( __FILE__ )."jv-ajax.js", array( 'jquery' ));
      wp_localize_script(
        'jv-ajax',
        'wp_ajax',
        array(
          'ajaxurl'      => admin_url( 'admin-ajax.php' ),
          'ajaxnonce'   => wp_create_nonce( 'ajax_post_validation' ),
          'post_ID' => get_the_ID()
        )
      );
    }



    /* Shortcode for blog post maps */
    public function shortcode_blog_post_maps($atts){
      $html = "<div id='jv-map-blog-post'></div>";
      return $html;
    }

    public function query_rand_post(){
      check_ajax_referer( 'ajax_post_validation', 'security' );
      $interest_points = get_field('interest_points',$_POST['post_ID']);
      $interest_points = explode('|', $interest_points);
      $json_interests = array();
      foreach($interest_points as $key => $interest_point){
        $json_interest[] = explode(',', $interest_point);
      }

      $json_data = array(
        'top' => get_field('top',$_POST['post_ID']),
        'left' => get_field('left',$_POST['post_ID']),
        'zoom' => get_field('zoom',$_POST['post_ID']),
        'points' => $json_interest
      );

      if( !isset( $json_data ) )
        wp_send_json_error( array( 'error' => __( 'Could not retrieve a post.' ) ) );
      else
        wp_send_json_success( json_encode($json_data) );
    }
  }
