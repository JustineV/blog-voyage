jQuery( document ).on('ready', function( )
{
  var data = {
    action: 'query_rand_post',
    security: wp_ajax.ajaxnonce,
    post_ID: wp_ajax.post_ID
  };

  if(jQuery('#jv-map-blog-post')){
    jQuery.post(
      wp_ajax.ajaxurl,
      data,
      function( response )
      {
        // ERROR HANDLING
        if( !response.success )
        {
          // No data came back, maybe a security error
          if( !response.data )
          jQuery( '#jv-map-blog-post' ).html( 'AJAX ERROR: no response' );
          else
          jQuery( '#jv-map-blog-post' ).html( response.data.error );
        }
        else {
          var json = JSON.parse(response.data);

          var icon = L.divIcon({
            className: 'leaflet-interactive',
            html: "<div style='background-color:#c30b82;' class='marker-pin'></div>",
            iconSize: [20, 20],
            iconAnchor: [10, 10]
          });

          var map = L.map('jv-map-blog-post').setView([json.top, json.left], json.zoom);
          L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 15,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          }).addTo(map);

          jQuery(json.points).each(function(){
            var marker = L.marker([jQuery(this)[0], jQuery(this)[1]], {icon: icon}).addTo(map);
            marker.bindPopup(jQuery(this)[2]);
          });

        }
      }
    );
  }

});
