<?php
/**
 * Widgets class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais Blog
 * @copyright  2021
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor;
use JvElementor\Widgets\Skins\Skin_Title_Side;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Class Plugin
 *
 * Main Plugin class
 *
 * @since 1.0.0
 */
class Widgets {

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @var Plugin The single instance of the class.
	 */
	private static $instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return Plugin An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Include Widgets files
	 *
	 * Load widgets files
	 *
	 * @since 1.0.0
	 * @access private
	 */
	private function include_widgets_files() {
		require_once 'widgets/slider.php';
		require_once 'widgets/slider-superposition.php';
		require_once 'widgets/title.php';
		require_once 'widgets/galerie.php';
		require_once 'widgets/line-galerie.php';
		require_once 'widgets/etape.php';
		require_once 'widgets/articles-recents.php';
		require_once 'widgets/categories-articles.php';
	}

	/**
   * Include Widgets skins
   *
   * Load widgets skins
   *
   * @since 0.0.1
   * @access private
   */
  private function include_skins_files() {
      require_once( __DIR__ . '/widgets/skins/categories-articles/skin-title-side.php') ;
			require_once( __DIR__ . '/widgets/skins/categories-articles/skin-carousel-title-side.php') ;
			require_once( __DIR__ . '/widgets/skins/articles-recents/skin-two-columns.php') ;
			require_once( __DIR__ . '/widgets/skins/articles-recents/skin-last-posts.php') ;
			require_once( __DIR__ . '/widgets/skins/articles-recents/skin-last-posts-one-column.php') ;
			require_once( __DIR__ . '/widgets/skins/articles-recents/skin-last-posts-no-bg.php') ;
  }

	/**
	 * Register Widgets
	 *
	 * Register new Elementor widgets.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function register_widgets() {
		// Its is now safe to include Widgets skins
    $this->include_skins_files();
    // Register skin
		//$widget->add_skin( new Skins\Skin_Title_Side($widget) );
    add_action( 'jv-elementor/widgets/categories-articles/skins_init', function( $widget ) {
       $widget->add_skin( new Skin_Title_Side($widget) );
			 $widget->add_skin( new Skin_Carousel_Title_Side($widget) );
			 $widget->add_skin( new Skin_Two_Columns($widget) );
    } );

		add_action( 'jv-elementor/widgets/articles-recents/skins_init', function( $widget ) {
			 $widget->add_skin( new Skin_Last_Posts($widget) );
			 $widget->add_skin( new Skin_Last_Posts_No_Bg($widget) );
			 $widget->add_skin( new Skin_Last_Posts_One_Column($widget) );
    } );

		// It's now safe to include Widgets files.
		$this->include_widgets_files();

		// Register the plugin widget classes.
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Slider() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SliderSuperposition() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Title() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Galerie() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Line_Galerie() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Etape() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Articles_Recents() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Categories_Articles() );


	}

	/**
	 *  Plugin class constructor
	 *
	 * Register plugin action hooks and filters
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function __construct() {
		// Register the widgets.
		add_action( 'elementor/widgets/widgets_registered', array( $this, 'register_widgets' ) );
	}
}

// Instantiate the Widgets class.
Widgets::instance();
