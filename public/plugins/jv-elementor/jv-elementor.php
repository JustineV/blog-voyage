<?php
/**
 * Jv Elementor WordPress Plugin
 *
 * @package JvElementor
 *
 * Plugin Name: Jv Elementor
 * Plugin URI : http://localhost/
 * Description: Get more widgets for Elementor
 * Version:     1.0.0
 * Author:      J'y vais blog
 * Author URI : http://localhost/
 * Text Domain: jv-elementor
 * File: jv-elementor.php
 */

define( 'JV_ELEMENTOR', __FILE__ );

/**
 * Include the Jv_Elementor class.
 */
require plugin_dir_path( JV_ELEMENTOR ) . 'class-jv-elementor.php';
