<?php
/**
 * Slider class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Plugin;
use Elementor\Group_Control_Image_Size;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Galerie extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'galerie', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'galerie';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Galerie', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	/**
	 * Enqueue styles.
	 */
	/*public function get_style_depends() {
		return array( 'galerie' );
	}*/

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'jv-galerie',
			[
				'label' => __( 'Ajouter Images', 'jv-elementor' ),
				'type' => Controls_Manager::GALLERY,
				'show_label' => false,
				'dynamic' => [
					'active' => true,
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$this->add_control(
			'liens_galerie',
			[
				'label' => __( 'Liens', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'file',
				'options' => [
					'file' => __( 'Media File', 'jv-elementor' ),
					'attachment' => __( 'Attachment Page', 'jv-elementor' ),
					'none' => __( 'None', 'jv-elementor' ),
				],
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'elementor' ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => __( 'https://your-link.com', 'elementor' ),
				'condition' => [
					'liens_galerie' => 'custom',
				],
				'show_label' => false,
			]
		);

		$this->add_control(
			'open_lightbox',
			[
				'label' => __( 'Lightbox', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'jv-elementor' ),
					'yes' => __( 'Yes', 'jv-elementor' ),
					'no' => __( 'No', 'jv-elementor' ),
				],
				'condition' => [
					'liens_galerie' => 'file',
				],
			]
		);

		$this->add_control(
			'caption_source',
			[
				'label' => __( 'Caption', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => __( 'None', 'elementor' ),
					'attachment' => __( 'Attachment Caption', 'elementor' )
				],
				'default' => 'none',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( ! $settings['jv-galerie'] ) {
			return;
		}


		$has_caption = ! empty( $settings['caption_source'] ) && 'none' !== $settings['caption_source'];
		$ids = wp_list_pluck( $settings['jv-galerie'], 'id' );

		$this->add_render_attribute( 'shortcode', 'ids', implode( ',', $ids ) );
		$this->add_render_attribute( 'shortcode', 'size', $settings['thumbnail_size'] );

		if ( $settings['liens_galerie'] ) {
			$this->add_render_attribute( 'shortcode', 'link', $settings['liens_galerie'] );
		}
		?>
		<div class="jv-elementor-image-galerie container">
			<?php
			add_filter( 'wp_get_attachment_link', [ $this, 'add_lightbox_data_to_image_link' ], 10, 2 );

			//echo do_shortcode( '[gallery ' . $this->get_render_attribute_string( 'shortcode' ) . ']' );


			//remove_filter( 'wp_get_attachment_link', [ $this, 'add_lightbox_data_to_image_link' ] );
			?>
		<div class="row">
			<div class="col-lg-6 row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12 jv-child-background">
							<div class="jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][0]['url'] ) ) { echo $settings['jv-galerie'][0]['url']; } ?>')">
								<?php if ( $has_caption ) : ?>
									<figure class="wp-caption">
								<?php endif; ?>
								<?php
								$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][0] );
								if ( $link ){
									if ( 'custom' !== $settings['liens_galerie'] ) {
										$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][0]['id'], $settings['open_lightbox'] );
									} ?>
									<a href="<?php if ( ! empty( $settings['jv-galerie'][0]['url'] ) ) { echo $settings['jv-galerie'][0]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
								<?php } ?>
								<?php if ( $has_caption ) : ?>
										<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][0] ); ?></figcaption>
									</figure>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 jv-child-background">
							<?php if(isset($settings['jv-galerie'][1])): ?>
								<div class="jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][1]['url'] ) ) { echo $settings['jv-galerie'][1]['url']; } ?>')">
									<?php if ( $has_caption ) : ?>
										<figure class="wp-caption">
									<?php endif; ?>
									<?php
									$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][1] );
									if ( $link ){
										if ( 'custom' !== $settings['liens_galerie'] ) {
											$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][1]['id'], $settings['open_lightbox'] );
										} ?>
										<a href="<?php if ( ! empty( $settings['jv-galerie'][1]['url'] ) ) { echo $settings['jv-galerie'][1]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
									<?php } ?>
									<?php if ( $has_caption ) : ?>
											<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][1] ); ?></figcaption>
										</figure>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
						<div class="col-md-4 jv-child-background">
							<div class="jv-elementor-color">

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 jv-child-background">
					<?php if(isset($settings['jv-galerie'][2])): ?>
						<div class="jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][2]['url'] ) ) { echo $settings['jv-galerie'][2]['url']; } ?>')">
							<?php if ( $has_caption ) : ?>
								<figure class="wp-caption">
							<?php endif; ?>
							<?php
							$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][2] );
							if ( $link ){
								if ( 'custom' !== $settings['liens_galerie'] ) {
									$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][2]['id'], $settings['open_lightbox'] );
								} ?>
								<a href="<?php if ( ! empty( $settings['jv-galerie'][2]['url'] ) ) { echo $settings['jv-galerie'][2]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
							<?php } ?>
							<?php if ( $has_caption ) : ?>
									<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][2] ); ?></figcaption>
								</figure>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="row">
					<div class="col-md-2 jv-child-background">
						<div class="jv-elementor-color">

						</div>
					</div>
					<div class="col-md-6 jv-child-background">
						<?php if(isset($settings['jv-galerie'][3])): ?>
							<div class="jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][3]['url'] ) ) { echo $settings['jv-galerie'][3]['url']; } ?>')">
								<?php if ( $has_caption ) : ?>
									<figure class="wp-caption">
								<?php endif; ?>
								<?php
								$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][3] );
								if ( $link ){
									if ( 'custom' !== $settings['liens_galerie'] ) {
										$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][3]['id'], $settings['open_lightbox'] );
									} ?>
									<a href="<?php if ( ! empty( $settings['jv-galerie'][3]['url'] ) ) { echo $settings['jv-galerie'][3]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
								<?php } ?>
								<?php if ( $has_caption ) : ?>
										<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][3] ); ?></figcaption>
									</figure>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12 jv-child-background">
								<div class="jv-elementor-color">

								</div>
							</div>
						</div>
						<div class="row jv-child-background">
							<?php if(isset($settings['jv-galerie'][4])): ?>
								<div class="col-md-12 jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][4]['url'] ) ) { echo $settings['jv-galerie'][4]['url']; } ?>')">
									<?php if ( $has_caption ) : ?>
										<figure class="wp-caption">
									<?php endif; ?>
									<?php
									$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][4] );
									if ( $link ){
										if ( 'custom' !== $settings['liens_galerie'] ) {
											$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][4]['id'], $settings['open_lightbox'] );
										} ?>
										<a href="<?php if ( ! empty( $settings['jv-galerie'][4]['url'] ) ) { echo $settings['jv-galerie'][4]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
									<?php } ?>
									<?php if ( $has_caption ) : ?>
											<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][4] ); ?></figcaption>
										</figure>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 jv-child-background">
						<?php if(isset($settings['jv-galerie'][5])): ?>
							<div class="jv-elementor-image" style="background-image:url('<?php if ( ! empty( $settings['jv-galerie'][5]['url'] ) ) { echo $settings['jv-galerie'][5]['url']; } ?>')">
								<?php if ( $has_caption ) : ?>
									<figure class="wp-caption">
								<?php endif; ?>
								<?php
								$link = $this->get_link_image_url( $settings, $settings['jv-galerie'][5] );
								if ( $link ){
									if ( 'custom' !== $settings['liens_galerie'] ) {
										$this->add_lightbox_data_attributes( 'link', $settings['jv-galerie'][5]['id'], $settings['open_lightbox'] );
									} ?>
									<a href="<?php if ( ! empty( $settings['jv-galerie'][5]['url'] ) ) { echo $settings['jv-galerie'][5]['url']; } ?>" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
								<?php } ?>
								<?php if ( $has_caption ) : ?>
										<figcaption class="widget-image-caption wp-caption-text"><?php echo $this->get_caption( $settings, $settings['jv-galerie'][5] ); ?></figcaption>
									</figure>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<div class="jv-elementor-image-galerie">
			<#
			_.each( settings.galerie, function( image, index ) {
					#>
				<div class="jv-galerie-img" style="background-image:url('
					<# if ( ! image.url ){ #>
							{{{ image.url }}}
					<# } #>
				')"></div>
				<#
					});
				#>
			</div>
		<?php
	}


	private function get_link_image_url( $settings, $image ) {

		if ( $settings['link'] ) {
			$this->add_link_attributes( 'link', $link );

			if ( Plugin::$instance->editor->is_edit_mode() ) {
				$this->add_render_attribute( 'link', [
					'class' => 'elementor-clickable',
				] );
			}


		}

		if ( 'none' === $settings['liens_galerie'] ) {
			return false;
		}

		if ( 'custom' === $settings['liens_galerie'] ) {
			if ( empty( $settings['link']['url'] ) ) {
				return false;
			}

			return $settings['link'];
		}

		return [
			'url' => $image['url'],
		];
	}

	private function get_caption( $settings, $image ) {
		$caption = '';
		if ( ! empty( $settings['caption_source'] ) ) {
			switch ( $settings['caption_source'] ) {
				case 'attachment':
					$caption = wp_get_attachment_caption( $image['id'] );
					break;
				case 'custom':
					$caption = ! Utils::is_empty( $settings['caption'] ) ? $settings['caption'] : '';
			}
		}
		return $caption;
	}
}
