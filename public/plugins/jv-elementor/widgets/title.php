<?php
/**
 * Title class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Title extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'title', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'title';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Title', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'heading_size',
			[
				'label' => __( 'HTML Tag', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
					'div' => 'div',
					'span' => 'span',
					'p' => 'p',
				],
				'default' => 'h2',
			]
		);

		$this->add_responsive_control(
			'heading_align',
			[
				'label' => __( 'Alignment', 'jv-elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'jv-elementor' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'jv-elementor' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'jv-elementor' ),
						'icon' => 'eicon-text-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'jv-elementor' ),
						'icon' => 'eicon-text-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'style',
			[
				'label' => __( 'Style', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'style-1',
				'options' => [
					'style-1' => __( 'Style 1', 'jv-elementor' ),
					'style-2' => __( 'Style 2', 'jv-elementor' ),
				]
			]
		);

		$this->add_control(
			'heading_title',
			array(
				'label'   => __( 'Title', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXTAREA,
				'default' => __( 'Title', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'heading_subtitle',
			array(
				'label'   => __( 'Subtitle', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXTAREA,
				'default' => __( 'Subtitle', 'jv-elementor' ),
			)
		);

		$this->end_controls_section();

		/* Style slide */
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text Color', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'global' => [
					'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
				],
				'selector' => '{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => __( 'Subtitle', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'subtitle_color',
			[
				'label' => __( 'Text Color', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-subtitle' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'subtitle_typography',
				'global' => [
					'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
				],
				'selector' => '{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-subtitle',
			]
		);


		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( '' === $settings['heading_title'] ) {
			return;
		}

		$this->add_render_attribute( 'heading_title', 'class', 'jv-elementor-heading-title' );
		$this->add_render_attribute( 'heading_subtitle', 'class', 'jv-elementor-heading-subtitle' );

		if ( ! empty( $settings['heading_size'] ) ) {
			$this->add_render_attribute( 'content', 'class', 'jv-elementor-size-' . $settings['heading_size'] );
		}

		$this->add_inline_editing_attributes( 'heading_title' );
		$this->add_inline_editing_attributes( 'heading_subtitle' );

		$title = $settings['heading_title'];
		$subtitle = $settings['heading_subtitle'];

		$title_html = "";

		$title_html .= sprintf( '<%1$s class="jv-elementor-heading '.$settings['style'].'"><span %2$s>%3$s</span><span %4$s>%5$s</span></%1$s>', $settings['heading_size'], $this->get_render_attribute_string( 'heading_subtitle' ), $subtitle, $this->get_render_attribute_string( 'heading_title' ), $title );


		echo $title_html;
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<#
		var heading_title = settings.heading_title;
		var heading_subtitle = settings.heading_subtitle;

		view.addRenderAttribute( 'content', 'class', [ 'jv-elementor-heading', 'jv-elementor-size-' + settings.heading_size ] );

		view.addInlineEditingAttributes( 'heading_title' );
		view.addInlineEditingAttributes( 'heading_subtitle' );

		var title_html = '<' + settings.heading_size  + ' class="jv-elementor-heading">'+'<span ' + view.getRenderAttributeString( 'heading_subtitle' ) + '>' + heading_subtitle + '</span><span ' + view.getRenderAttributeString( 'heading_title' ) + '>' + heading_title + '</span></' + settings.heading_size + '>';

		print( title_html );
		#>
		<?php
	}
}
