<?php
/**
 * Slider class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Slider extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'slider', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'slider';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Slider', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	/**
	 * Enqueue styles.
	 */
	/*public function get_style_depends() {
		return array( 'slider' );
	}*/

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		/* Begin repeater */
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'slide_title',
			array(
				'label'   => __( 'Title', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'Title', 'jv-elementor' ),
			)
		);

		$repeater->add_control(
			'slide_subtitle',
			array(
				'label'   => __( 'Subtitle', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'Subtitle', 'jv-elementor' ),
			)
		);

		$repeater->add_control(
			'slide_image_en_avant',
			[
				'label' => __( 'Image en avant', 'jv-elementor' ),
				'type' => Controls_Manager::MEDIA,
        'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$repeater->add_control(
			'slide_description',
			[
				'label' => '',
				'type' => Controls_Manager::WYSIWYG,
				'default' => '<p>' . esc_html__( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor' ) . '</p>',
			]
		);

		$repeater->add_control(
			'slide_link',
			[
				'label' => __( 'Link', 'jv-elementor' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( '#', 'jv-elementor' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$repeater->add_control(
			'slide_background',
			[
				'label' => __( 'Background', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .jv-elementor-content' => 'background-color: {{VALUE}};',
				],
			]
		);

		/* End repeater */

		$this->add_control(
			'slides',
			[
				'label' => __( '', 'jv-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'slide_title' => __( 'Slide #1', 'jv-elementor' ),
						'slide_subtitle' => __( 'Subtitle', 'jv-elementor' )
					],
					[
						'slide_title' => __( 'Slide #2', 'jv-elementor' ),
						'slide_subtitle' => __( 'Subtitle', 'jv-elementor' )
					]
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();


		/* Style slide */
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Slide', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		?>
		<div class="jv-elementor-slider">
			<div class="swiper-container gallery-top">
				<div class="swiper-wrapper">
				<?php
					foreach($settings['slides'] as $index => $slide) :
						$slide_count = $index + 1;

						$slide_title_setting_key = $this->get_repeater_setting_key( 'slide_title', 'slides', $index );

						$this->add_render_attribute( $slide_title_setting_key );
						?>
						<div class="jv-elementor-slide swiper-slide" style="background-image:url('<?php if ( ! empty( $slide['slide_image_en_avant']['url'] ) ) { echo $slide['slide_image_en_avant']['url']; } ?>')">
							<div class="jv-elementor-background-mobile "  style="background-image:url('<?php if ( ! empty( $slide['slide_image_en_avant']['url'] ) ) { echo $slide['slide_image_en_avant']['url']; } ?>')"></div>
							<div class="jv-elementor-content">
								<p class="jv-elementor-titles" <?php //echo $this->get_render_attribute_string( $slide_title_setting_key ); ?>>
									<span class="jv-elementor-slide-subtitle"><?php echo $slide['slide_subtitle'] ?></span>
									<span class="jv-elementor-slide-title"><?php echo $slide['slide_title'] ?></span>
								</p>
								<div class="jv-elementor-description">
									<?php echo $slide['slide_description']; ?>
								</div>
								<?php if ( ! empty( $slide['slide_link']['url'] ) ) { ?>
									<a href="'<?php echo $slide['slide_link']['url']?>'" class="btn btn-border-white">Lire</a>
								<?php } ?>
							</div>
						</div>
					<?php endforeach; ?>
					</div>
			</div>
			<div class="swiper-container gallery-thumbs">
				<div class="swiper-wrapper">
					<?php
						foreach($settings['slides'] as $index2 => $slide) :
							$slide_count = $index2 + 1;

							$slide_title_setting_key = $this->get_repeater_setting_key( 'slide_title', 'slides', $index2 );

							$this->add_render_attribute( $slide_title_setting_key );
							?>
							<div class="jv-elementor-slide swiper-slide" style="background-image:url('<?php if ( ! empty( $slide['slide_image_en_avant']['url'] ) ) { echo $slide['slide_image_en_avant']['url']; } ?>')"></div>
						<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<!--<#
		view.addInlineEditingAttributes( 'title', 'none' );
		view.addInlineEditingAttributes( 'description', 'basic' );
		view.addInlineEditingAttributes( 'content', 'advanced' );
		#>
		<h2 {{{ view.getRenderAttributeString( 'title' ) }}}>{{{ settings.title }}}</h2>
		<div {{{ view.getRenderAttributeString( 'description' ) }}}>{{{ settings.description }}}</div>
		<div {{{ view.getRenderAttributeString( 'content' ) }}}>{{{ settings.content }}}</div>-->

		<div class="jv-elementor-slider">
			<div class="swiper-container gallery-top">
				<div class="swiper-wrapper">
					<#
					_.each( settings.slides, function( slide, index ) {
						var slideCount = index + 1,
						  slideTitleKey = view.getRepeaterSettingKey( 'slide_title', 'slides', index );

							view.addRenderAttribute( slideTitleKey );
							#>
						<div class="jv-elementor-slide swiper-slide" style="background-image:url('
								<# if ( ! slide.slide_image_en_avant.url ){ #>
									{{{ slide.slide_image_en_avant.url }}}
								<# } #>
							')"></div>
							<div class="jv-elementor-background-mobile "  style="background-image:url('
									<# if ( ! slide.slide_image_en_avant.url ){ #>
										{{{ slide.slide_image_en_avant.url }}}
									<# } #>
								')"></div>
							<div class="jv-elementor-content">
								<h2>
									<span class="jv-elementor-slide-subtitle">{{{ slide.slide_subtitle }}}</span>
									<span class="jv-elementor-slide-title">{{{ slide.slide_title }}}</span>
								</h2>
								<div class="jv-elementor-description">
									{{{ slide.slide_description }}}
								</div>
								<# if ( ! slide.slide_link.url ){ #>
									<a href="'{{{ slide.slide_link.url }}}'" class="btn btn-border-white">Lire</a>
								<# } #>
							</div>
						</div>
					<#
						});
					#>
					</div>
			</div>
			<div class="swiper-container gallery-thumbs">
				<div class="swiper-wrapper">
					<#
					_.each( settings.slides, function( slide, index2 ) {
							#>
							<div class="jv-elementor-slide swiper-slide" style="background-image:url('
							<# if ( ! slide.slide_image_en_avant.url ){ #>
								{{{ slide.slide_image_en_avant.url }}}
							<# } #>
							')">
						</div>
					<#
						});
					#>
				</div>
			</div>
		</div>'

		<?php
	}
}
