<?php
/**
 * Title class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Icons_Manager;
use JvElementor\Widgets\Skins;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Categories_Articles extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'categories_articles', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'categories_articles';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Liste des catégories', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	protected function register_skins() {
		$this->add_skin( new Skins\Skin_Title_Side( $this ) );
		$this->add_skin( new Skins\Skin_Carousel_Title_Side( $this ) );
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		/* Begin repeater */
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'category_subtitle',
			array(
				'label'   => __( 'Subtitle', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'Subtitle', 'jv-elementor' ),
			)
		);

		$repeater->add_control(
			'category_title',
			array(
				'label'   => __( 'Title', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'Title', 'jv-elementor' ),
			)
		);

		$repeater->add_control(
			'category_icon',
			[
				'label' => __( 'Icon', 'jv-elementor' ),
				'type' => Controls_Manager::ICONS,
				'fa4compatibility' => 'icon',
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'fa-solid',
				],
			]
		);

		$repeater->add_control(
			'category_image_en_avant',
			[
				'label' => __( 'Image en avant', 'jv-elementor' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$repeater->add_control(
			'category_link',
			[
				'label' => __( 'Link', 'jv-elementor' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( '#', 'jv-elementor' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		/* End repeater */

		$this->add_control(
			'categories',
			[
				'label' => __( 'Categories', 'jv-elementor' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'category_title' => __( 'Normandie', 'jv-elementor' )
					]
				],
				'title_field' => '{{{ category_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_columns',
			array(
				'label' => __( 'Colonnes', 'jv-elementor' ),
			)
		);

		$text_columns = range( 1, 12 );
		$text_columns = array_combine( $text_columns, $text_columns );
		$text_columns[''] = __( '', 'jv-elementor' );

		$this->add_control(
			'columns',
			[
				'label' => __( 'Columns', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => $text_columns,
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		?>
		<div class="jv-elementor-categories-articles">
			<div class="row">
				<?php
				foreach ($settings['categories'] as $index => $category){
					?>
					<div class="col-lg-<?php echo $settings['columns']; ?>">
						<div class="jv-vignette">
							<div class="jv-vignette-bg" style="background-image:url('<?php echo $category['category_image_en_avant']['url']; ?>')">
								<a href="<?php echo $category["category_link"]["url"]; ?>" class="jv-category"></a>
							</div>
							<a href="<?php echo $category["category_link"]["url"]; ?>" class="jv-category">
								<div class="jv-group-title">
									<?php
										Icons_Manager::render_icon( $category['category_icon'], [ 'aria-hidden' => 'true' ] );
									?>
									<h3 class="jv-category_name"><span class="jv-subtitle"><?php echo $category['category_subtitle'] ?></span> <span class="jv-title"><?php echo $category['category_title'] ?></span></h3>
								</div>
							</a>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<?php
	}
}
