<?php
/**
 * Title class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Articles_Recents extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'articles_recents', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'articles_recents';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Articles Récents', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	protected function register_skins() {
		$this->add_skin( new Skins\Skin_Two_Columns( $this ) );
		$this->add_skin( new Skins\Skin_Last_Posts( $this ) );
		$this->add_skin( new Skins\Skin_Last_Posts_No_Bg( $this ) );
		$this->add_skin( new Skins\Skin_Last_Posts_One_Column( $this ) );
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */

		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			)
		);

		$this->add_control(
			'category_ids',
				[
						'label' => __('Category','jv-elementor'),
						'type' => Controls_Manager::SELECT2,
						'multiple' => true,
						'object_type' => 'category',
						'options' => wp_list_pluck(get_terms('category'), 'name', 'term_id'),
				]
		);

		$this->add_control(
				'posts_per_page',
				[
						'label' => __('Posts Per Page', 'jv-elementor'),
						'type' => Controls_Manager::NUMBER,
						'default' => '4',
				]
		);

		$this->add_control(
		     'orderby',
		      [
		        'label' => __('Order By', 'jv-elementor'),
		        'type' => Controls_Manager::SELECT,
						'options' =>  array(
		            'ID' => 'Post ID',
		            'author' => 'Post Author',
		            'title' => 'Title',
		            'date' => 'Date',
		        ),
		        'default' => 'date',
          ]
    );

		 $this->add_control(
		      'order',
		      [
		        'label' => __('Order', 'jv-elementor'),
		        'type' => Controls_Manager::SELECT,
		        'options' => [
		             'asc' => 'Ascending',
		              'desc' => 'Descending',
		        ],
		        'default' => 'desc',
        	]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings();
		$settings = wp_parse_args($settings, [
				'post_type' => 'post',
				'posts_ids' => [],
				'orderby' => 'date',
				'order' => 'desc',
				'posts_per_page' => 3,
				'offset' => 0,
				'post__not_in' => [],
		]);

		$args = [
				'orderby' => $settings['orderby'],
				'order' => $settings['order'],
				'ignore_sticky_posts' => 1,
				'post_status' => 'publish',
				'posts_per_page' => $settings['posts_per_page'],
				'offset' => $settings['offset'],
		];

		if ('by_id' === $settings['post_type']) {
				$args['post_type'] = 'any';
				$args['post__in'] = empty($settings['posts_ids']) ? [0] : $settings['posts_ids'];
		} else {
				$args['post_type'] = $settings['post_type'];

				//if ($args['post_type'] !== 'page') {
						$args['tax_query'] = [];

						$taxonomies = get_object_taxonomies($settings['post_type'], 'objects');

						foreach ($taxonomies as $object) {
								$setting_key = $object->name . '_ids';

								if (!empty($settings[$setting_key])) {
										$args['tax_query'][] = [
												'taxonomy' => $object->name,
												'field' => 'term_id',
												'terms' => $settings[$setting_key],
										];
								}
						}

						if (!empty($args['tax_query'])) {
								$args['tax_query']['relation'] = 'AND';
						}
				//}
		}

		$query = new \WP_Query( $args );
		if ( $query->have_posts() ) {
			?>
			<div class="jv-elementor-articles-recents jv-skin-default">
				<div class="row">
					<?php
						while ( $query->have_posts() ) {
								$query->the_post();
								?>
								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
									<a href="<?php the_permalink(); ?>">
										<div class="jv-article"  style="background-image:url('<?php echo esc_url(wp_get_attachment_image_url(get_post_thumbnail_id(), 'full')); ?>')">
											<div class="jv-group-title">
												<?php
													$terms = get_the_category();
													$last_index = (sizeof($terms)-1);
													echo "<i class='iconjv-".$terms[$last_index]->slug."'></i>";
												 ?>
												<h3 class="jv-title"><?php echo get_the_title(); ?></h3>
												<div class="jv-description"><?php echo get_the_excerpt(); ?></div>
											</div>
										</div>
									</a>
								</div>
								<?php

						}?>
				</div>
			</div>
			<?php
		}
	}
}
