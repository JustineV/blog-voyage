<?php
/**
 * Slider class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Plugin;
use Elementor\Group_Control_Image_Size;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Line_Galerie extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'line_galerie', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'line_galerie';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Line Galerie', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'jv-galerie',
			[
				'label' => __( 'Ajouter Images', 'jv-elementor' ),
				'type' => Controls_Manager::GALLERY,
				'show_label' => false,
				'dynamic' => [
					'active' => true,
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		if ( ! $settings['jv-galerie'] ) {
			return;
		}

		?>
		<div class="jv-elementor-image-line-galerie">
			<div class="jv-content-image">
				<div class="container-line-galerie">
					<!--<div class="swiper-wrapper">-->
						<?php
						foreach($settings['jv-galerie'] as $key_galerie => $image) {
							?>
								<div class="container-image-line-galerie">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo get_post_meta($image['id'], '_wp_attachment_image_alt', TRUE); ?>" />
								</div>
							<?php
						}
						?>
					<!--</div>-->
				</div>
			</div>
		</div>
		<?php
	}


}
