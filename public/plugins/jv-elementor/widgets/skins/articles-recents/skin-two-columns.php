<?php
namespace JvElementor\Widgets\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Two_Columns extends Skin_Base {

	public function __construct( Widget_Base $parent ) {
		// define parent widget (button)
		$this->parent = $parent;
	}

	/**
	 * Get skin ID.
	 *
	 * Retrieve the skin ID.
	 *
	 * @since 1.0.0
	 * @access public
	 * @abstract
	 */
	public function get_id() {
		return 'skin-two-columns';
	}

	/**
	 * Get skin title.
	 *
	 * Retrieve the skin title.
	 *
	 * @since 1.0.0
	 * @access public
	 * @abstract
	 */
	public function get_title() {
		return __( 'Two columns', 'jv-elementor-custom-skins' );
	}

	/**
	 * Render button widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	 public function render() {
 		$settings = $this->parent->get_settings();
 		$settings = wp_parse_args($settings, [
 				'post_type' => 'post',
 				'posts_ids' => [],
 				'orderby' => 'date',
 				'order' => 'desc',
 				'posts_per_page' => 3,
 				'offset' => 0,
 				'post__not_in' => [],
 		]);

 		$args = [
 				'orderby' => $settings['orderby'],
 				'order' => $settings['order'],
 				'ignore_sticky_posts' => 1,
 				'post_status' => 'publish',
 				'posts_per_page' => $settings['posts_per_page'],
 				'offset' => $settings['offset'],
 		];

 		if ('by_id' === $settings['post_type']) {
 				$args['post_type'] = 'any';
 				$args['post__in'] = empty($settings['posts_ids']) ? [0] : $settings['posts_ids'];
 		} else {
 				$args['post_type'] = $settings['post_type'];

 				//if ($args['post_type'] !== 'page') {
 						$args['tax_query'] = [];

 						$taxonomies = get_object_taxonomies($settings['post_type'], 'objects');

 						foreach ($taxonomies as $object) {
 								$setting_key = $object->name . '_ids';

 								if (!empty($settings[$setting_key])) {
 										$args['tax_query'][] = [
 												'taxonomy' => $object->name,
 												'field' => 'term_id',
 												'terms' => $settings[$setting_key],
 										];
 								}
 						}

 						if (!empty($args['tax_query'])) {
 								$args['tax_query']['relation'] = 'AND';
 						}
 				//}
 		}

 		$query = new \WP_Query( $args );
 		if ( $query->have_posts() ) {
 			?>
 			<div class="jv-elementor-articles-recents jv-skin-two-columns">
 				<div class="row">
 					<?php
 						while ( $query->have_posts() ) {
 								$query->the_post();
 								?>
 								<div class="jv-article col-md-6">
 									<a href="<?php echo get_the_permalink(); ?>">
 										<div class="jv-image"  style="background-image:url('<?php echo esc_url(wp_get_attachment_image_url(get_post_thumbnail_id(), 'full')); ?>')">

 										</div>
									</a>
									<div class="jv-icon">
										<?php
											$terms = get_the_category();
											$name_category = "";
											foreach($terms as $index => $term){
												echo "<a href='".site_url()."/".$term->slug."/'><i class='iconjv-".$term->slug."'></i></a>";
												$name_category .= "<a href='".site_url()."/".$term->slug."/'>".$term->name."</a>";
												if($index != 0){
													$name_category .= ",";
												}
											}
										 ?>
									</div>
									<div class="jv-content">
										<div class="jv-metadonnees"><span class="jv-date"><?php echo get_the_date(); ?></span> — <span class="jv-category"><?php echo $name_category; ?></span></div>
										<h3 class="jv-title"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
										<div class="jv-description"><?php echo get_the_excerpt(); ?></div>
										<a href="<?php get_the_permalink(); ?>" class="jv-link">Lire l'article</a>
									</div>
 								</div>
 								<?php

 						}?>
 				</div>
 			</div>
 			<?php
 		}
 	}


}
