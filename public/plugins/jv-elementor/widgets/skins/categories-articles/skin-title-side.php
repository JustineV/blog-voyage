<?php
namespace JvElementor\Widgets\Skins;

use Elementor\Widget_Base;
use Elementor\Skin_Base;
use Elementor\Controls_Manager;
use Elementor\Icons_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Skin_Title_Side extends Skin_Base {

	public function __construct( Widget_Base $parent ) {
		// define parent widget (button)
		$this->parent = $parent;
	}

	/**
	 * Get skin ID.
	 *
	 * Retrieve the skin ID.
	 *
	 * @since 1.0.0
	 * @access public
	 * @abstract
	 */
	public function get_id() {
		return 'skin-title-side';
	}

	/**
	 * Get skin title.
	 *
	 * Retrieve the skin title.
	 *
	 * @since 1.0.0
	 * @access public
	 * @abstract
	 */
	public function get_title() {
		return __( 'Title Side', 'jv-elementor-custom-skins' );
	}

	/**
	 * Render button widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function render() {
    $settings = $this->parent->get_settings_for_display();
		?>
		<div class="jv-elementor-categories-articles skin-title-side">
			<div class="row">
				<?php
				foreach ($settings['categories'] as $index => $category){
					?>
					<div class="col-md-<?php echo $settings['columns']; ?>">
						<div class="jv-vignette">
              <a href="<?php echo $category["category_link"]["url"]; ?>" class="jv-category">
                <div class="jv-group-title">
									<div></div>
                  <h3 class="jv-title"><?php echo $category['category_title'] ?></h3>
                  <?php
                    Icons_Manager::render_icon( $category['category_icon'], [ 'aria-hidden' => 'true' ] );
                  ?>
                </div>
              </a>
							<div class="jv-vignette-bg" style="background-image:url('<?php echo $category['category_image_en_avant']['url']; ?>')">
								<a href="<?php echo $category["category_link"]["url"]; ?>" class="jv-category"></a>
							</div>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<?php
	}


}
