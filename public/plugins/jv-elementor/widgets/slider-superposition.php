<?php

/**
 * Slider class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;

// Security Note: Blocks direct access to the plugin PHP files.
defined('ABSPATH') || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class SliderSuperposition extends Widget_Base
{
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct($data = array(), $args = null)
	{
		parent::__construct($data, $args);
		wp_register_style('slider-superposition', plugins_url('/assets/css/style.css', JV_ELEMENTOR), array(), '1.0.0');
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'slider-superposition';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __('Slider Superposition', 'jv-elementor');
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return ['jy-vais'];
	}

	/**
	 * Enqueue styles.
	 */
	/*public function get_style_depends() {
		return array( 'slider' );
	}*/

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls()
	{
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __('Content', 'jv-elementor'),
			)
		);

		/* Begin repeater */
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'slide_title',
			array(
				'label'   => __('Title', 'jv-elementor'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Title', 'jv-elementor'),
			)
		);

		$repeater->add_control(
			'slide_subtitle',
			array(
				'label'   => __('Subtitle', 'jv-elementor'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Subtitle', 'jv-elementor'),
			)
		);

		$repeater->add_control(
			'slide_image_back',
			[
				'label' => __('Image back', 'jv-elementor'),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_control(
			'slide_image_front',
			[
				'label' => __('Image front', 'jv-elementor'),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				]
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `thumbnail_size` and `thumbnail_custom_dimension`.
				'default' => 'full',
				'separator' => 'none',
			]
		);

		$repeater->add_control(
			'slide_description',
			[
				'label' => '',
				'type' => Controls_Manager::WYSIWYG,
				'default' => '<p>' . esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor') . '</p>',
			]
		);

		$repeater->add_control(
			'slide_link',
			[
				'label' => __('Link', 'jv-elementor'),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __('#', 'jv-elementor'),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$repeater->add_control(
			'bottom',
			array(
				'label'   => __('Bottom', 'jv-elementor'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Bottom', 'jv-elementor'),
			)
		);

		$repeater->add_control(
			'left',
			array(
				'label'   => __('Left', 'jv-elementor'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Left', 'jv-elementor'),
			)
		);

		$repeater->add_control(
			'slide_id',
			array(
				'label'   => __('Slide ID', 'jv-elementor'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Slide ID', 'jv-elementor'),
			)
		);

		/* End repeater */

		$this->add_control(
			'slides',
			[
				'label' => __('', 'jv-elementor'),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'slide_title' => __('Slide #1', 'jv-elementor'),
						'slide_subtitle' => __('Subtitle', 'jv-elementor')
					],
					[
						'slide_title' => __('Slide #2', 'jv-elementor'),
						'slide_subtitle' => __('Subtitle', 'jv-elementor')
					]
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();


		/* Style slide */
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __('Slide', 'jv-elementor'),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();

?>
		<div class="jv-elementor-slider jv-elementor-slider-superposition">
			<div class="swiper-container gallery-top">
				<div class="swiper-wrapper">
					<?php
					foreach ($settings['slides'] as $index => $slide) :
						$slide_count = $index + 1;

						$slide_title_setting_key = $this->get_repeater_setting_key('slide_title', 'slides', $index);

						$this->add_render_attribute($slide_title_setting_key);
					?>
						<div class="jv-elementor-slide swiper-slide" id="<?php echo $slide['slide_id']; ?>">

							<div class="jv-elementor-image-back" style="background-image:url('<?php echo $slide['slide_image_back']['url']; ?>')"></div>

							<div class="jv-elementor-content" style="bottom:<?php echo $slide['bottom']; ?>; left:<?php echo $slide['left']; ?>;">
								<p class="jv-elementor-titles" <?php //echo $this->get_render_attribute_string( $slide_title_setting_key ); 
																?>>
									<span class="jv-elementor-slide-title"><?php echo $slide['slide_title'] ?></span>
									<span class="jv-elementor-slide-subtitle"><?php echo $slide['slide_subtitle'] ?></span>
									<?php if (!empty($slide['slide_link']['url'])) { ?>
										<a href="<?php echo $slide['slide_link']['url']; ?>" class="btn-link-slide">Lire la suite</a>
									<?php } ?>
								</p>
								<div class="jv-elementor-description">
									<?php
									if (!empty($slide['slide_description'])) {
										echo $slide['slide_description'];
									}
									?>
								</div>
							</div>

							<div class="jv-elementor-image-front" style="background-image:url('<?php echo $slide['slide_image_front']['url']; ?>')"></div>
							<?php if (!empty($slide['slide_link']['url'])) { ?>
								<a href="<?php echo $slide['slide_link']['url']; ?>" class="link-slide"></a>
							<?php } ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="swiper-container gallery-thumbs">
				<div class="swiper-wrapper">
					<?php
					foreach ($settings['slides'] as $index2 => $slide) :
						$slide_count = $index2 + 1;

						$slide_title_setting_key = $this->get_repeater_setting_key('slide_title', 'slides', $index2);

						$this->add_render_attribute($slide_title_setting_key);
					?>
						<div class="jv-elementor-slide swiper-slide" style="background-image:url('<?php if (!empty($slide['slide_image_back']['url'])) {
																										echo $slide['slide_image_back']['url'];
																									} ?>')"></div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
<?php
	}
}
