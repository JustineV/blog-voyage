<?php
/**
 * Title class.
 *
 * @category   Class
 * @package    JvElementor
 * @subpackage WordPress
 * @author     J'y vais blog
 * @copyright  2020 J'y vais blog
 * @since      1.0.0
 * php version 7.3.9
 */

namespace JvElementor\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Utils;
use Elementor\WYSIWYG;
use Elementor\Control_Media;
use Elementor\Group_Control_Image_Size;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Jv Elementor widget class.
 *
 * @since 1.0.0
 */
class Etape extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );
		wp_register_style( 'etape', plugins_url( '/assets/css/style.css', JV_ELEMENTOR ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'etape';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Etape', 'jv-elementor' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'jy-vais' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function register_controls() {
		/* Title of the tab */
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'heading_size',
			[
				'label' => __( 'HTML Tag', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
					'div' => 'div',
					'span' => 'span',
					'p' => 'p',
				],
				'default' => 'h2',
			]
		);

		$this->add_responsive_control(
			'heading_align',
			[
				'label' => __( 'Alignment', 'jv-elementor' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'jv-elementor' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'jv-elementor' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'jv-elementor' ),
						'icon' => 'eicon-text-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'jv-elementor' ),
						'icon' => 'eicon-text-align-justify',
					],
				],
				'default' => '',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'style',
			[
				'label' => __( 'Style', 'jv-elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'image-left',
				'options' => [
					'image-left' => __( 'Image à gauche', 'jv-elementor' ),
					'image-right' => __( 'Image à droite', 'jv-elementor' ),
				]
			]
		);

		$this->add_control(
			'title',
			array(
				'label'   => __( 'Title', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXTAREA,
				'default' => __( 'Title', 'jv-elementor' ),
			)
		);

		$this->add_control(
			'subtitle',
			array(
				'label'   => __( 'Subtitle', 'jv-elementor' ),
				'type'    => Controls_Manager::TEXTAREA,
				'default' => __( 'Subtitle', 'jv-elementor' ),
			)
		);

    $this->add_control(
			'description',
			[
				'label' => '',
				'type' => Controls_Manager::WYSIWYG,
				'default' => '<p>' . __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'elementor' ) . '</p>',
			]
		);

		$this->add_control(
			'jv-galerie',
			[
				'label' => __( 'Ajouter Images', 'jv-elementor' ),
				'type' => Controls_Manager::GALLERY,
				'show_label' => false,
				'dynamic' => [
					'active' => true,
				],
			]
		);

    $this->add_control(
			'map',
			[
				'label' => __( 'Choose Map', 'jv-elementor' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->end_controls_section();

		/* Style slide */
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text Color', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'global' => [
					'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
				],
				'selector' => '{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => __( 'Subtitle', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'subtitle_color',
			[
				'label' => __( 'Text Color', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-subtitle' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'subtitle_typography',
				'global' => [
					'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
				],
				'selector' => '{{WRAPPER}} .jv-elementor-heading .jv-elementor-heading-subtitle',
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'jv-elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'description_color',
			[
				'label' => __( 'Text Color', 'jv-elementor' ),
				'type' => Controls_Manager::COLOR,
				'global' => [
					'default' => Global_Colors::COLOR_PRIMARY,
				],
				'selectors' => [
					'{{WRAPPER}} .jv-content-description' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'description_typography',
				'global' => [
					'default' => Global_Typography::TYPOGRAPHY_PRIMARY,
				],
				'selector' => '{{WRAPPER}} .jv-content-description',
			]
		);


		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$html = "";



		/*if ( '' === $settings['heading_title'] ) {
			return;
		}

		$this->add_render_attribute( 'heading_title', 'class', 'jv-elementor-heading-title' );
		$this->add_render_attribute( 'heading_subtitle', 'class', 'jv-elementor-heading-subtitle' );*/

		if ( ! empty( $settings['heading_size'] ) ) {
			$this->add_render_attribute( 'content', 'class', 'jv-elementor-size-' . $settings['heading_size'] );
		}

		$this->add_inline_editing_attributes( 'title' );
		$this->add_inline_editing_attributes( 'subtitle' );

		$title = $settings['title'];
		$subtitle = $settings['subtitle'];

		$title_html = "";
		$image_html = "";
		$map_html = "";

		$title_html .= sprintf( '<%1$s class="jv-elementor-heading '.$settings['style'].'"><span %2$s>%3$s</span><span %4$s>%5$s</span></%1$s>', $settings['heading_size'], $this->get_render_attribute_string( 'heading_subtitle' ), $subtitle, $this->get_render_attribute_string( 'heading_title' ), $title );

		foreach( $settings['jv-galerie'] as $index => $image) {
			$image_url = Group_Control_Image_Size::get_attachment_image_src( $image['id'], 'thumbnail', $settings );
			$image_html .= '<div class="swiper-slide"><img class="jv-image" src="' . esc_attr( $image_url ) . '" alt="' . esc_attr( Control_Media::get_image_alt( $image ) ) . '" /></div>';
		}



		$map_html .= "<div class='jv-map' style='background-image:url(".$settings['map']['url'].")'></div>";

		if($settings['style'] == 'image-right'){
			?>
			<div class="row">
				<div class="col-xl-4 col-lg-6 col-sm-12 content-left">
					<div class="jv-content-title">
						<?php echo $title_html; ?>
						<div class="jv-border-top"></div>
					</div>
					<div class="jv-content">
						<div class="jv-content-description">
							<?php echo $settings['description']; ?>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-lg-6 col-sm-12 content-left">
					<div class="jv-content-map">
						<?php echo $map_html; ?>
					</div>
					<div class="jv-content-image swiper-container">
						<div class="swiper-wrapper">
							<?php echo $image_html; ?>
						</div>
						<div class="swiper-pagination"></div>
  					<div class="swiper-button-prev"></div>
  					<div class="swiper-button-next"></div>
					</div>
				</div>
			</div>
			<?php
		}
		elseif($settings['style'] == 'image-left'){
		?>
			<div class="row">
				<div class="col-xl-8 col-lg-6 col-sm-12 content-right">
					<div class="jv-content-map">
						<?php echo $map_html; ?>
					</div>
					<div class="jv-content-image swiper-container">
						<div class="swiper-wrapper">
							<?php echo $image_html; ?>
						</div>
						<div class="swiper-pagination"></div>
  					<div class="swiper-button-prev"></div>
  					<div class="swiper-button-next"></div>
					</div>
				</div>
				<div class="col-xl-4 col-lg-6 col-sm-12 content-right">
					<div class="jv-content-title">
						<div class="jv-border-top"></div>
						<?php echo $title_html; ?>
					</div>
					<div class="jv-content">
						<div class="jv-content-description">
							<?php echo $settings['description']; ?>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
	}

}
