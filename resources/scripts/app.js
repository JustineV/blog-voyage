import Swiper from 'swiper/bundle';
import anime from 'animejs/lib/anime.es.js';
import { gsap } from "gsap";
// or get other plugins:
//import Draggable from "gsap/Draggable";
import ScrollTrigger from "gsap/ScrollTrigger";
// or all tools are exported from the "all" file (excluding bonus plugins):
//import { gsap, ScrollTrigger, Draggable, MotionPathPlugin } from "gsap/all";
//console.log(anime);

gsap.registerPlugin(ScrollTrigger);

jQuery(document).on('ready', function () {

  // HEADER
  //Searchbar
  jQuery(window).on('click', function(e){
    if(jQuery('#searchform').hasClass('open')){
      if(e.target.className != "material-icons search" && e.target.parentElement.className != "search-custom-form d-flex"){
        jQuery('#searchform').removeClass('open');
      }      
    }
  });

  jQuery('.header .searchbar i.search').on('click', function(){
    jQuery('#searchform').toggleClass('open');
    if(jQuery('#searchform').hasClass('open')){
      jQuery('.header .searchbar input[type="text"]').trigger('focus');
    }    
  });

  jQuery(window).on('scroll', function () {
    var scroll = jQuery(window).scrollTop();
    if (scroll >= 100) {
      jQuery('.header').css('position', 'fixed');
      jQuery('.header').addClass('fixed');
    } else {
      jQuery('.header').removeClass('fixed');
    }

  });

  // SLIDER
  if (jQuery('.jv-elementor-slider .swiper-container').length) {
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 10,
      slidesPerView: 3,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });

    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 17,
      autoplay: {
        delay: 5000,
      },
      loop: true,
      thumbs: {
        swiper: galleryThumbs
      },
    });

    if(jQuery(window).width() > 767){    
      var JvElementorSlideTitles = jQuery('.gallery-top .swiper-slide .jv-elementor-titles');
      JvElementorSlideTitles.each(function () {
        jQuery(this).children('.jv-elementor-slide-subtitle').html(jQuery(this).children('.jv-elementor-slide-subtitle').text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
        jQuery(this).children('.jv-elementor-slide-title').html(jQuery(this).children('.jv-elementor-slide-title').text().replace(/\S/g, "<span class='letter'>$&</span>").replace(/>\s</g, "><span class='letter'>&nbsp;</span><"));
      });
    }

    // ANIMEjs
  /*if (jQuery('.jv-elementor-titles .jv-elementor-slide-title').length > 0) {
    var JvElementorSlideTitle = document.querySelectorAll('.jv-elementor-titles .jv-elementor-slide-title');
    JvElementorSlideTitle.forEach(function (title) {
      title.innerHTML = title.textContent.replace(/\s|\S/g, "<span class='letter'>$&</span>");
      anime.timeline()
        .add({
          targets: '.jv-elementor-slide-title .letter',
          scale: [0, 1],
          duration: 1500,
          elasticity: 600,
          delay: (el, i) => 45 * (i + 1)
        });
    });
  }*/

    /*anime.timeline({loop: true})
      .add({
        targets: '.swiper-slide .jv-elementor-slide-title .letter',
        scale: [0, 1],
        duration: 1500,
        elasticity: 600,
        delay: (el, i) => 45 * (i + 1)
      });*/
    if(jQuery(window).width() > 767){
      var animation_subtitle = anime.timeline({loop: true})
        .add({
          targets: '.swiper-slide .jv-elementor-slide-subtitle .letter',
          opacity: [0, 1],
          easing: "easeOutExpo",
          duration: 750,
          offset: '-=775',
          delay: (el, i) => 34 * (i + 1)
        }).add({
          targets: '.swiper-slide .jv-elementor-slide-subtitle',
          opacity: 0,
          duration: 50,
          easing: "easeOutExpo",
          delay: 1000
        });
      

      

      galleryTop.on('slideChange', function () {
        if (jQuery('.swiper-slide-active .jv-elementor-slide-subtitle').length > 0) {
          jQuery('.swiper-slide .jv-elementor-slide-subtitle .letter').pause;
          jQuery('.swiper-slide-active .jv-elementor-slide-subtitle .letter').play;
          /*anime.timeline({ loop: true })
            .add({
              targets: '.swiper-slide-active .jv-elementor-slide-subtitle .letter',
              opacity: [0, 1],
              easing: "easeOutExpo",
              duration: 600,
              offset: '-=775',
              delay: (el, i) => 34 * (i + 1)
            }).add({
              targets: '.swiper-slide-active .jv-elementor-slide-subtitle',
              opacity: 0,
              duration: 200,
              easing: "easeOutExpo",
              delay: 1000
            });*/

          anime.timeline()
            .add({
              targets: '.swiper-slide-active .jv-elementor-slide-title .letter',
              scale: [0, 1],
              duration: 1500,
              elasticity: 600,
              delay: (el, i) => 45 * (i + 1)
            });
        }
      });
    }
  }

  // WIDGET ETAPES
  if (jQuery('.jv-etape .jv-content-image .swiper-container').length) {
    jQuery('.jv-etape .jv-content-image .swiper-container').each(function (index) {
      jQuery(this).parent().addClass('jv-content-image-' + index);
      var jv_content_image = new Swiper('.jv-content-image-' + index + ' .swiper-container', {
        loop: true,
        centeredSlides: true,
        centeredSlidesBounds: true,
        setWrapperSize: true,
        autoplay: {
          delay: 5000,
        },
        effect: 'fade',
        fadeEffect: {
          crossFade: true
        },
        pagination: {
          el: '.jv-content-image-' + index + ' .swiper-pagination',
          type: 'bullets',
          clickable: true,
        },
        navigation: {
          nextEl: '.jv-content-image-' + index + ' .swiper-button-next',
          prevEl: '.jv-content-image-' + index + ' .swiper-button-prev',
        },
      });
    });

  }

  // Single post - See more description
  if (jQuery('.post-description').length) {
    jQuery('.post-description').on('click', function (e) {
      jQuery(this).toggleClass('active');
      if (jQuery(this).find('span')[0].innerHTML == "+") {
        jQuery(this).find('span')[0].innerHTML = "–";
        jQuery(this).children('.post-description-content').css('max-height', jQuery(this).children('.post-description-content').prop('scrollHeight') + 'px');

      }
      else {
        jQuery(this).find('span')[0].innerHTML = "+";
        jQuery(this).children('.post-description-content').css('max-height', '0px');
      }
    });
  }

  // WIDGET ARTICLES RECENTS
  var widthArticlesRecents = jQuery('.jv-elementor-articles-recents .jv-article').width();
  jQuery('.jv-elementor-articles-recents .jv-article').height(widthArticlesRecents);
  jQuery('.jv-elementor-articles-recents.jv-skin-default .jv-article').height(widthArticlesRecents * 1.5);
  jQuery('.jv-elementor-articles-recents.jv-skin-default .jv-group-title').each(function () {
    var height_group_title = jQuery(this).height();
    jQuery(this).parent().css('margin-bottom', height_group_title / 2 + 60);
  });

  // WIDGET CATEGORIE ARTICLES - SKIN TITLE SIDE
  /*var skin_title_side = jQuery('.jv-elementor-categories-articles.skin-title-side .jv-vignette');
  if( skin_title_side.length ){
    jQuery.each(skin_title_side, function(index){
      var width_title_side = jQuery(this).width()*90/100;
      jQuery(this).css('height',width_title_side);
      jQuery(this).parent().css('height',width_title_side);
    });
  }*/

  // WIDGET CATEGORIE ARTICLES - SKIN CAROUSEL TITLE SIDE
  if (jQuery('.jv-elementor-categories-articles.skin-carousel-title-side').length) {
    var jv_carousel_title_side = new Swiper('.jv-elementor-categories-articles.skin-carousel-title-side', {
      //loop: true,
      slidesPerView: 1,
      spaceBetween: 30,
      /*autoplay: {
        delay: 5000,
      },*/
      breakpoints: {
        575: {

        },
        768: {
          slidesPerView: 2,
        },
        1200: {
          slidesPerView: 3,
        }
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      on: {
        afterInit: function () {
          jQuery.each(this.slides, function (index) {
            var width_title_side = jQuery(this).width() * 90 / 100;
            jQuery(this).css('height', width_title_side);
            jQuery(this).parent().css('height', width_title_side);
          });
        }
      }
    });
  }

  // LINE Galerie
  if (jQuery('.jv-elementor-image-line-galerie .jv-content-image .container-line-galerie').length) {
    /*var jv_content_image_line_galerie = new Swiper('.jv-elementor-image-line-galerie .jv-content-image .swiper-container', {
      slidesPerView: 6,
      spaceBetween: 60,
      //loop: true,
      /*autoplay: {
       delay: 2000,
     },*/
    /*breakpoints: {
      0: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      575: {
        slidesPerView: 4,
        spaceBetween: 30
      },
      1024: {
        slidesPerView: 6,
        spaceBetween: 60
      }
    }
  });*/
  }


  // GSAP
  /*  gsap.registerPlugin( ScrollTrigger, SplitText);
  let split;
  let animation =  gsap.timeline({});

  function init(){

  }

  init();*/

  // TOOLTIP ARCHIVE MAP
  jQuery('.article-map-polaroid').each(function () {
    var tooltip_id = jQuery(this).data('id');
    var tooltip_left = jQuery(this).data('left');
    var tooltip_top = jQuery(this).data('top');
    var tooltip_name = jQuery(this).data('name');
    var tooltip_slug = jQuery(this).attr('id');
    var article_link = jQuery(this).children('a').attr('href');
    jQuery('<div class="article-map-interest" style="top:' + tooltip_top + '%; left:' + tooltip_left + '%;" data-slug="' + tooltip_slug + '"><div class="article-map-tooltip" data-id="' + tooltip_id + '"></div><div class="article-map-legend"><a href="#' + tooltip_slug + '">' + tooltip_name + '</a></div></div>').appendTo('.article-map-image');
  });

  jQuery('.article-map-tooltip').on('mouseover', function () {
    var tooltip_id = jQuery(this).data('id');
    jQuery('#post-' + tooltip_id).toggleClass('hover');
  });

  jQuery(document).on("mousemove", ".jv-elementor-slide", function (e) {
    if(jQuery(window).width() > 767){
      var element = jQuery('.jv-elementor-content');
      var elementPos = jQuery('.jv-elementor-content').position();
      elementPos.top = elementPos.top + (element.innerWidth() - element.width());
      elementPos.left = elementPos.left + (element.innerHeight() - element.height());

      var x = e.clientX;
      var y = e.clientY;
      var newposX = x;
      newposX = (x - elementPos.left) * 0.2;
      var newposY = y;;
      newposY = (y - elementPos.top) * 0.2;
      jQuery(".jv-elementor-titles").css("transform", "translate3d(" + newposX + "px," + newposY + "px,0px)");
    }
  });

  // ACCORDION
  jQuery('.accordion-item').on('click', function (e) {
    if (jQuery(this).hasClass('active')) {
      jQuery(this).removeClass('active');
      jQuery(this).find('.accordion-collapse').hide();
    }
    else {
      var id = jQuery(this).attr('id');
      jQuery('.article-map-interest').removeClass('active');
      jQuery('.article-map-interest[data-slug="' + id + '"]').addClass('active');
      jQuery('.accordion-item').removeClass('active');
      jQuery('.accordion-item .accordion-collapse').hide();
      jQuery(this).addClass('active');
      jQuery(this).find('.accordion-collapse').show();
    }
  });

  jQuery('.article-map-interest').on('click', function (e) {
    jQuery('.article-map-interest').removeClass('active');
    jQuery(this).addClass('active');
    var data_id = jQuery(this).find('.article-map-tooltip').data('id');
    jQuery('.accordion-item').removeClass('active');
    jQuery('.accordion-item .accordion-collapse').hide();
    jQuery('.accordion-item[data-id="' + data_id + '"]').addClass('active');
    jQuery('.accordion-item[data-id="' + data_id + '"] .accordion-collapse').show();
  });



  if (jQuery('.post-breadcrumb .post-title').length > 0) {
    var JvBreadcrumbsPostTitle = document.querySelector('.post-breadcrumb .post-title');
    JvBreadcrumbsPostTitle.innerHTML = JvBreadcrumbsPostTitle.textContent.replace(/[A-Za-záàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ0-9-,:;.?!']*/g, "<span class='word'>$&</span>");
    var JvBreadcrumbsPostTitleWord = jQuery('.post-breadcrumb .post-title .word');
    JvBreadcrumbsPostTitleWord.each(function (index) {
      if (JvBreadcrumbsPostTitleWord[index].textContent.length == 0) {
        //jQuery(this).toggleClass('word space');
      }
      else {
        //JvBreadcrumbsPostTitleWord[index].innerHTML = JvBreadcrumbsPostTitleWord[index].textContent.replace(/\S/g, "<span class='letter'>$&</span>");
      }
      jQuery('.post-title .space').each(function () {
        //jQuery(this).text(" ");
      });
    });


    anime.timeline()
      .add({
        targets: '.post-breadcrumb .post-title .word',
        scale: [0, 1],
        duration: 1500,
        elasticity: 600,
        delay: (el, i) => 45 * (i + 1)
      });
  }



  // GSAP
  // Animate titles
  if ((jQuery('.elementor-widget-title').length > 0) && (jQuery('.elementor-widget-title .jv-elementor-heading-title').length > 0)) {
    const titles = gsap.utils.toArray('.elementor-widget-title');
    const heading_titles = gsap.utils.toArray('.elementor-widget-title .jv-elementor-heading-title');
    heading_titles.forEach((heading_title, i) => {
      /*gsap.to(heading_title, {
        scrollTrigger: {
          trigger: heading_title,
          //toggleActions: 'restart pause reverse pause',
          start: "-300px center",
          end: "300px center",
          scrub: 1,
        },
        x: 400,
        duration: 3,
      });*/

      if (jQuery(window).width() > 768) {
        gsap.timeline({
          scrollTrigger: {
            trigger: heading_title,
            //toggleActions: 'restart pause reverse pause',
            start: "-200px center",
            end: "200px center",
            scrub: 1,
            //ease: 'Power1.easeOut',
          },
          defaults: {
            x: -50,
            duration: 6,
          }
        })
          .from(heading_title, {
            x: -50,
          })
          .to(heading_title, {
            x: 50,
          });
      }
    });
  }

  if ((jQuery('.jv-elementor-heading').length > 0) && (jQuery('.jv-elementor-heading .jv-elementor-heading-title').length > 0) && (jQuery(window).width() > 768)) {
    const jv_titles = gsap.utils.toArray('.jv-elementor-heading');
    const jv_heading_titles = gsap.utils.toArray('.jv-elementor-heading .jv-elementor-heading-title');
    jv_heading_titles.forEach((jv_heading_title, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: jv_heading_title,
          //toggleActions: 'restart pause reverse pause',
          start: "-200px center",
          end: "200px center",
          scrub: 1,
          //ease: 'Power1.easeOut',
        },
        defaults: {
          x: -50,
          duration: 6,
        }
      })
        .from(jv_heading_title, {
          x: -50,
        })
        .to(jv_heading_title, {
          x: 50,
        });
    });
  }


  // Animate little gallery
  if ((jQuery('.elementor-widget-line_galerie').length > 0) && (jQuery(window).width() > 767)) {
    var width_galerie = jQuery('.elementor-widget-line_galerie .container-line-galerie').width() / 3;
    const line_galerie = gsap.utils.toArray('.elementor-widget-line_galerie .container-line-galerie');
    line_galerie.forEach((line_galerie, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: line_galerie,
          start: "-250px center",
          end: "300px center",
          scrub: 1,
        },
        defaults: {
          duration: 6
        }
      })
        .from(line_galerie, {
          x: "250px",
        })
        .to(line_galerie, {
          x: - width_galerie,
        });
    });
  }

  if ((jQuery('.elementor-widget-line_galerie').length > 0) && (jQuery(window).width() < 768)) {
    var width_galerie = jQuery('.elementor-widget-line_galerie .container-line-galerie').width() / 3;
    const line_galerie = gsap.utils.toArray('.elementor-widget-line_galerie .container-line-galerie');
    line_galerie.forEach((line_galerie, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: line_galerie,
          start: "-350px center",
          end: "350px center",
          scrub: 1,
          //markers: true,
        },
        defaults: {
          duration: 6,
          x: 0,
        }
      })
        .from(line_galerie, {
          x: 0,
        })
        .to(line_galerie, {
          x: -width_galerie,
        });
    });
  }

  if (jQuery('.elementor-widget-line_galerie .container-image-line-galerie').length > 0) {
    const line_galerie_odd = gsap.utils.toArray('.elementor-widget-line_galerie .container-image-line-galerie:nth-child(odd)');

    line_galerie_odd.forEach((line_galerie_odd, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: line_galerie_odd,
          //toggleActions: 'restart pause reverse pause',
          start: "-200px center",
          end: "300px center",
          scrub: 1,
          //markers: true
        },
        defaults: {
          duration: 6,
          y: -30
        }
      })
        .to(line_galerie_odd, {
          y: 30,
        });
    });

    const line_galerie_even = gsap.utils.toArray('.elementor-widget-line_galerie .container-image-line-galerie:nth-child(even)');

    line_galerie_even.forEach((line_galerie_even, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: line_galerie_even,
          //toggleActions: 'restart pause reverse pause',
          start: "-200px center",
          end: "300px center",
          scrub: 1,
        },
        defaults: {
          y: 30,
          duration: 6,
        }
      })
        .to(line_galerie_even, {
          y: -30,
        });
    });
  }


  // Anim Gsap Articles Recents
  if (jQuery('.jv-elementor-articles-recents.jv-skin-last-posts-no-bg .col-xl-4').length > 0 && jQuery(window).width() > 1200) {
    const articles_recents_group_1 = gsap.utils.toArray('.jv-elementor-articles-recents.jv-skin-last-posts-no-bg .col-xl-4:nth-child(3n+2)');
    const articles_recents_group_2 = gsap.utils.toArray('.elementor-widget-articles_recents .col-xl-4:not(:nth-child(3n+2))');

    articles_recents_group_1.forEach((articles_recents_group_1, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: '.jv-elementor-articles-recents.jv-skin-last-posts-no-bg',
          //toggleActions: 'restart pause reverse pause',
          start: "top center",
          end: "bottom center",
          scrub: 1,
          ease: 'Power1.easeOut',
        },
        defaults: {
          duration: 6,
        }
      })
        .to(articles_recents_group_1, {
          y: -200,
        });
    });

    articles_recents_group_2.forEach((articles_recents_group_2, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: '.jv-elementor-articles-recents.jv-skin-last-posts-no-bg',
          //toggleActions: 'restart pause reverse pause',
          start: "-250px center",
          end: "top center",
          scrub: 1,
          ease: 'Power1.easeOut',
        },
        defaults: {
          duration: 6,
        }
      })
        .to(articles_recents_group_2, {
          y: 200,
        });
    });
  }

  /*if(jQuery('.jv-bg-parallax .elementor-background-overlay').length > 0){
    const jv_bg_parallax = gsap.utils.toArray('.jv-bg-parallax');
    const jv_bg_parallax_overlay = gsap.utils.toArray('.jv-bg-parallax .elementor-background-overlay');

    jv_bg_parallax_overlay.forEach((jv_bg_parallax_overlay, i) => {
      jv_bg_parallax_overlay.style.backgroundPositionY = `150px`;
      gsap.timeline({
        scrollTrigger: {
          trigger: jv_bg_parallax[i],
          //toggleActions: 'restart pause reverse pause',
          scrub: 1
        },
        defaults: {
          scale: 1,
        }
      })
        .to(jv_bg_parallax_overlay, {
          backgroundPositionY: `300px`,
          scale: 2,
        });
      });
    }*/

  if (jQuery('.elementor-widget-slider-superposition .jv-elementor-image-back').length > 0) {
    const jv_slider_superposition = gsap.utils.toArray('.elementor-widget-slider-superposition');
    const jv_slider_superposition_image = gsap.utils.toArray('.elementor-widget-slider-superposition .jv-elementor-image-back');

    jv_slider_superposition_image.forEach((jv_slider_superposition_image, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: jv_slider_superposition[i],
          //toggleActions: 'restart pause reverse pause',
          scrub: 1,
          start: '-50px center',
        },
        defaults: {
          scale: 1,
        }
      })
        .to(jv_slider_superposition_image, {
          scale: 2,
        });
    });
  }

  if (jQuery('.jv-bg-parallax ').length > 0) {
    const jv_slider_superposition_parallax = gsap.utils.toArray('.jv-bg-parallax');
    const jv_slider_superposition_image_parallax = gsap.utils.toArray('.jv-bg-parallax .elementor-background-overlay');

    jv_slider_superposition_image_parallax.forEach((jv_slider_superposition_image_parallax, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: jv_slider_superposition_parallax[i],
          //toggleActions: 'restart pause reverse pause',
          scrub: 1,
          start: '-50px center',
        },
        /*defaults: {
          scale: 1,
        }*/
      }).from(jv_slider_superposition_image_parallax, {
        scale:1.2,
      })
        .to(jv_slider_superposition_image_parallax, {
          scale: 1,
        });
    });
  }

  if (jQuery('.article-map').length > 0) {
    gsap.timeline({
      scrollTrigger: {
        trigger: '.article-map',
        //toggleActions: 'restart pause reverse pause',
        scrub: 1,
        start: '-200px center',
        end: '100px center',
      }
    })
      .from('.article-map-image', {
        y: -100,
        x: -100,
      })
      .to('.article-map-image', {
        y: 0,
        x: 0,
      });

    /*gsap.timeline({
      scrollTrigger: {
        trigger: '.article-map',
        //toggleActions: 'restart pause reverse pause',
        scrub: 1,
        start: '-200px center',
        end: '100px center',
      }
    })
      .from('.article-map-polaroid', {
        y: -100,
        x: 100
      })
      .to('article-map-polaroid', {
        y: 0,
        x: 0,
      });*/
  }

  if (jQuery('.jv-elementor-articles-recents .jv-article').length > 0) {
    const articles_recents_group = gsap.utils.toArray('.jv-elementor-articles-recents.jv-skin-last-posts-one-column');
    const articles_recents_group_1 = gsap.utils.toArray('.jv-elementor-articles-recents.jv-skin-last-posts-one-column .jv-article:nth-child(even)');
    const articles_recents_group_2 = gsap.utils.toArray('.jv-elementor-articles-recents.jv-skin-last-posts-one-column .jv-article:nth-child(odd)');

    /*articles_recents_group.forEach((articles_recents_group, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: articles_recents_group,
          //toggleActions: 'restart pause reverse pause',
          start: "-400px center",
          end: "top center",
          scrub: 1,
          ease: 'none',
        }
      })
        .to(articles_recents_group, {
          y: -1000,
        });
      });*/

    articles_recents_group_1.forEach((articles_recents_group_1, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: articles_recents_group_1,
          //toggleActions: 'restart pause reverse pause',
          start: "-150px center",
          end: "200px center",
          scrub: 1,
          ease: 'none'
        }
      })
        .from(articles_recents_group_1, {
          x: 400,
        })
        .to(articles_recents_group_1, {
          x: 0,
        });
    });

    articles_recents_group_2.forEach((articles_recents_group_2, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: articles_recents_group_2,
          //toggleActions: 'restart pause reverse pause',
          start: "-150px center",
          end: "200px center",
          scrub: 1,
          ease: 'none',
        }
      })
        .from(articles_recents_group_2, {
          x: -400,
        })
        .to(articles_recents_group_2, {
          x: 0,
        });
    });
  }

  if (jQuery('.single-post .citation-quote').length > 0) {
    const jv_citation_quotes = gsap.utils.toArray('.single-post .citation-quote');

    jv_citation_quotes.forEach((jv_citation_quote, i) => {
      gsap.timeline({
        scrollTrigger: {
          trigger: '.single-post .intro .col-xl-6:last-child ',
          scrub: 1,
          start: '-50px center',
        }
      })
        .from(jv_citation_quote, {
          y: -20,
        })
        .to(jv_citation_quote, {
          y: 10,
        });
    });
  }

  jQuery('#back-to-top').on('click', function () {
    jQuery("html, body").animate({ scrollTop: 0 }, 1000);
  });

  jQuery('#navbarHeader input').on('click', function(){
    jQuery('.header').toggleClass('open');
  });
});
